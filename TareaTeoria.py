import pulp as pl

# Definicion de las plantas de origen
plantas_origen = ["CdValles", "Tampico", "Ebano"]

# Valores dados a las plantas de origen
produccion_origen = { "CdValles": 4000, "Tampico": 5000, "Ebano": 3000}

# Definicion de las plantas de destino
plantas_destino = ["Queretaro", "Monterrey", "Veracruz", "Zacatecas"]

# Valores dados a las plantas de destino
produccion_destino = {"Queretaro": 2000, "Monterrey": 45000, "Veracruz": 3000, "Zacatecas": 2500}

# Matriz de costos
costos = [
    #Queretaro, Monterrey, Veracruz, Zacatecas
    [4, 7, 5, 9],# Cd. valles
    [6, 4, 8, 5],# Tampico
    [4, 6, 6, 8],# Ebano
]

# Transferencia de costos a un diccionario de pulp para su posterior uso
costos = pl.makeDict([plantas_origen, plantas_destino], costos, 0)

# creacion de variable que almanacena el problema de transporte a minimizar
problema = pl.LpProblem("problema_transporte", pl.LpMinimize)

# creacion variable rutas como una lista sobre listas que almacena en i las plantas origen
# y en j las plantas destino
ruta = [(i, j) for i in plantas_origen for j in plantas_destino]

# Crea las variables con la funcion pulp que junta plantas de origen con destino para crear distintas rutas
variables = pl.LpVariable.dicts("Ruta", (plantas_origen, plantas_destino), 0, None, pl.LpInteger)

# variable problema que suma variables con costos en ruta
problema += pl.lpSum([variables[i][j] * costos[i][j] for (i,j) in ruta]), "Costo_suma"

# For que rellenan las listas con los datos de plantas origen y plantas destinos
for i in plantas_origen:
    problema += (
        pl.lpSum([variables[i][j] for j in plantas_destino]) <= produccion_origen[i],
        "Suma_promedio_plantas_origen_%s" % i,
    )

for j in plantas_destino:
    problema += (
        pl.lpSum([variables[i][j] for i in plantas_origen]) == produccion_destino[j],
        "Suma_promedio_plantas_origen_%s" % j,
    )

# codigo para solucionar el problema
problema.solve(pl.PULP_CBC_CMD(msg=False))

# impresion del estado siendo este viable o inviable
print("Status:", pl.LpStatus[problema.status])
# impresion de las rutas con los valores obtenidos al resolver el problema
for v in problema.variables():
    if v.varValue > 0:
        print(v.name, "=", v.varValue)

# impresion del costo de transporte
print("total de costo de tranporte: ", pl.value(problema.objective))